{
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "schemes": [
    "https"
  ],
  "swagger": "2.0",
  "info": {
    "description": "A simple API allowing consumers to view biometric, sleep and\nworkout data from Biostrap users.\n\nIn order to obtain access to the API you need to register your\napplication by sending an e-mail to `developers@biostrap.com`\nwith your application name and its redirect URL.",
    "title": "Biostrap Public API",
    "contact": {
      "name": "Developer contact",
      "email": "developers@biostrap.com"
    },
    "version": "1.0.0",
    "x-logo": {
      "url": "/static/logo_heart_on_black.png",
      "altText": "Biostrap logo"
    }
  },
  "host": "api-beta.biostrap.com",
  "basePath": "/v1",
  "paths": {
    "/activities": {
      "get": {
        "produces": [
          "application/json"
        ],
        "tags": [
          "activities"
        ],
        "summary": "Returns paginated workout stats.",
        "operationId": "getActivities",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "x-go-name": "LastTimestamp",
            "description": "Last timestamp (with milisecond precision) of previous page (or 0 if first page).",
            "name": "last-timestamp",
            "in": "query",
            "required": true
          },
          {
            "maximum": 50,
            "type": "integer",
            "format": "int64",
            "x-go-name": "Limit",
            "description": "Maximum amount of results per page.",
            "name": "limit",
            "in": "query",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/activitiesResponse"
          },
          "400": {
            "$ref": "#/responses/activitiesParameterErrorResponse"
          }
        }
      }
    },
    "/biometrics": {
      "get": {
        "produces": [
          "application/json"
        ],
        "tags": [
          "biometrics"
        ],
        "summary": "Returns paginated biometrics.",
        "operationId": "getBiometrics",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "x-go-name": "LastTimestamp",
            "description": "Last timestamp (with milisecond precision) of previous page (or 0 if first page).",
            "name": "last-timestamp",
            "in": "query",
            "required": true
          },
          {
            "maximum": 50,
            "type": "integer",
            "format": "int64",
            "x-go-name": "Limit",
            "description": "Maximum amount of results per page.",
            "name": "limit",
            "in": "query",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/biometricsResponse"
          },
          "400": {
            "$ref": "#/responses/genericParameterErrorResponse"
          }
        }
      }
    },
    "/insights": {
      "get": {
        "produces": [
          "application/json"
        ],
        "tags": [
          "insights"
        ],
        "summary": "Returns insights for a given day.",
        "operationId": "getInsights",
        "parameters": [
          {
            "type": "string",
            "example": "2018-10-24",
            "x-go-name": "Date",
            "description": "The date to filter with, in ISO 8601 format.",
            "name": "date",
            "in": "query",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/insightsResponse"
          },
          "400": {
            "$ref": "#/responses/genericParameterErrorResponse"
          }
        }
      }
    },
    "/monitoring": {
      "get": {
        "produces": [
          "application/json"
        ],
        "tags": [
          "monitoring"
        ],
        "summary": "Returns a list of users you monitor and their information.",
        "operationId": "getMonitoringInfo",
        "responses": {
          "200": {
            "$ref": "#/responses/monitoringInfoResponse"
          }
        }
      }
    },
    "/sleep": {
      "get": {
        "produces": [
          "application/json"
        ],
        "tags": [
          "sleep"
        ],
        "summary": "Returns data for the sleep sessions of a given day.",
        "operationId": "getSleepStats",
        "parameters": [
          {
            "type": "string",
            "example": "2018-10-24",
            "x-go-name": "Date",
            "description": "The date to filter with, in ISO 8601 format.",
            "name": "date",
            "in": "query",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/sleepStatsResponse"
          },
          "400": {
            "$ref": "#/responses/genericParameterErrorResponse"
          }
        }
      }
    },
    "/steps": {
      "get": {
        "produces": [
          "application/json"
        ],
        "tags": [
          "steps"
        ],
        "summary": "Returns paginated per-minute physical stats.",
        "operationId": "getSteps",
        "parameters": [
          {
            "type": "integer",
            "format": "int64",
            "x-go-name": "LastTimestamp",
            "description": "Last timestamp (with milisecond precision) of previous page (or 0 if first page).",
            "name": "last-timestamp",
            "in": "query",
            "required": true
          },
          {
            "maximum": 50,
            "type": "integer",
            "format": "int64",
            "x-go-name": "Limit",
            "description": "Maximum amount of results per page.",
            "name": "limit",
            "in": "query",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "$ref": "#/responses/stepsResponse"
          },
          "400": {
            "$ref": "#/responses/stepsParameterErrorResponse"
          }
        }
      }
    },
    "/user": {
      "get": {
        "produces": [
          "application/json"
        ],
        "tags": [
          "users"
        ],
        "summary": "Returns the user's profile information.",
        "operationId": "getUser",
        "responses": {
          "200": {
            "$ref": "#/responses/userResponse"
          },
          "404": {
            "$ref": "#/responses/userNotFoundResponse"
          }
        }
      }
    }
  },
  "definitions": {
    "ActivitiesResponseBody": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/WorkoutStats"
          },
          "x-go-name": "Data"
        },
        "links": {
          "$ref": "#/definitions/PaginationLinks"
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "Activity": {
      "type": "object",
      "properties": {
        "active_time": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "ActiveTime"
        },
        "activity_equipment": {
          "type": "string",
          "x-go-name": "ActivityEquipment"
        },
        "activity_variation": {
          "type": "string",
          "x-go-name": "ActivityVariation"
        },
        "bad_reps": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "BadReps"
        },
        "breaks": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Breaks"
        },
        "calories": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Calories"
        },
        "cardio_metrics": {
          "$ref": "#/definitions/CardioMetrics"
        },
        "consistency": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Consistency"
        },
        "distance": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Distance"
        },
        "duration": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Duration"
        },
        "end_time": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "EndTime"
        },
        "form": {
          "$ref": "#/definitions/ActivityFormQuality"
        },
        "good_reps": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "GoodReps"
        },
        "is_corrected": {
          "type": "boolean",
          "x-go-name": "IsCorrected"
        },
        "likely_name": {
          "type": "string",
          "x-go-name": "LikelyName"
        },
        "muscle_target": {
          "$ref": "#/definitions/ActivityMuscleTarget"
        },
        "potential_matches": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "x-go-name": "PotentialMatches"
        },
        "rep_unit": {
          "$ref": "#/definitions/ActivityRepUnit"
        },
        "rep_velocity": {
          "type": "number",
          "format": "float",
          "x-go-name": "RepVelocity"
        },
        "reps": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Reps"
        },
        "rps": {
          "type": "number",
          "format": "float",
          "x-go-name": "RPS"
        },
        "score": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Score"
        },
        "start_time": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "StartTime"
        },
        "swimming_metrics": {
          "$ref": "#/definitions/SwimMetrics"
        },
        "weight": {
          "type": "number",
          "format": "float",
          "x-go-name": "Weight"
        },
        "weight_unit": {
          "$ref": "#/definitions/ActivityWeightUnit"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "ActivityFormQuality": {
      "$ref": "#/definitions/Activity_FormQuality"
    },
    "ActivityMuscleTarget": {
      "$ref": "#/definitions/Activity_MuscleTarget"
    },
    "ActivityRepUnit": {
      "$ref": "#/definitions/Activity_RepUnit"
    },
    "ActivityWeightUnit": {
      "$ref": "#/definitions/Activity_WeightUnit"
    },
    "Activity_FormQuality": {
      "description": "Form of the activity",
      "type": "integer",
      "format": "int32",
      "x-go-package": "biostrap/proto"
    },
    "Activity_MuscleTarget": {
      "description": "Muscle targets of the movement",
      "type": "integer",
      "format": "int32",
      "x-go-package": "biostrap/proto"
    },
    "Activity_RepUnit": {
      "description": "Reps units for apps to show",
      "type": "integer",
      "format": "int32",
      "x-go-package": "biostrap/proto"
    },
    "Activity_WeightUnit": {
      "description": "unit of weight",
      "type": "integer",
      "format": "int32",
      "x-go-package": "biostrap/proto"
    },
    "BiometricsResponseBody": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/CardioStats"
          },
          "x-go-name": "Data"
        },
        "links": {
          "$ref": "#/definitions/PaginationLinks"
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "CardioMetrics": {
      "type": "object",
      "properties": {
        "cadence": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Cadence"
        },
        "calories_per_minute": {
          "type": "array",
          "items": {
            "type": "number",
            "format": "float"
          },
          "x-go-name": "CaloriesPerMinute"
        },
        "distance_per_minute": {
          "type": "array",
          "items": {
            "type": "integer",
            "format": "int32"
          },
          "x-go-name": "DistancePerMinute"
        },
        "environment": {
          "$ref": "#/definitions/CardioMetricsEnvironment"
        },
        "is_trivial": {
          "type": "boolean",
          "x-go-name": "IsTrivial"
        },
        "rep_per_minute": {
          "type": "array",
          "items": {
            "type": "integer",
            "format": "int32"
          },
          "x-go-name": "RepPerMinute"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "CardioMetricsEnvironment": {
      "$ref": "#/definitions/CardioMetrics_Environment"
    },
    "CardioMetrics_Environment": {
      "description": "Activity environment",
      "type": "integer",
      "format": "int32",
      "x-go-package": "biostrap/proto"
    },
    "CardioStats": {
      "type": "object",
      "properties": {
        "bpm": {
          "description": "Heartbeats per minute.",
          "type": "number",
          "format": "float",
          "x-go-name": "BPM",
          "example": 80
        },
        "brpm": {
          "description": "Breaths per minute.",
          "type": "number",
          "format": "float",
          "x-go-name": "BRPM",
          "example": 25
        },
        "hrv": {
          "description": "Heart rate variability (rMSSD).",
          "type": "number",
          "format": "float",
          "x-go-name": "HRV",
          "example": 25
        },
        "resting_bpm": {
          "type": "number",
          "format": "float",
          "x-go-name": "RestingBPM"
        },
        "resting_hrv": {
          "type": "number",
          "format": "float",
          "x-go-name": "RestingHRV"
        },
        "spo2": {
          "description": "Arterial oxygen saturation.\nThe unit is the percentage of oxygenated haemoglobin in\na red blood cell.",
          "type": "number",
          "format": "float",
          "x-go-name": "SPO2",
          "example": 98
        },
        "state": {
          "description": "Describes wether this was an automated recording\nwhile the user was resting or sleeping, or if it was\nmanually requested by the user from an MA session.\nPossible values are \"resting\", \"sleeping\" and \"manual\".",
          "type": "string",
          "x-go-name": "State",
          "example": "resting"
        },
        "timestamp": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "Timestamp",
          "example": 1523905147000
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "GenericParameterError": {
      "type": "object",
      "properties": {
        "detail": {
          "type": "string",
          "x-go-name": "Detail"
        },
        "source": {
          "$ref": "#/definitions/ParameterErrorSource"
        },
        "status": {
          "description": "The HTTP status code for this error.",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Status"
        },
        "title": {
          "type": "string",
          "x-go-name": "Title"
        }
      },
      "x-go-package": "biostrap/api/common/jsoncommon"
    },
    "Goals": {
      "type": "object",
      "required": [
        "steps",
        "sleep",
        "calories",
        "workout"
      ],
      "properties": {
        "calories": {
          "description": "Calories to burn per day",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Calories",
          "example": 2300
        },
        "sleep": {
          "description": "Seconds of sleep per day",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Sleep",
          "example": 28800
        },
        "steps": {
          "description": "Steps to walk per day",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Steps",
          "example": 10000
        },
        "workout": {
          "description": "Seconds of workout per day",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Workout",
          "example": 3600
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "Insight": {
      "type": "object",
      "properties": {
        "comment": {
          "type": "string",
          "x-go-name": "Comment"
        },
        "content": {
          "type": "string",
          "x-go-name": "Content"
        },
        "graph": {
          "type": "object",
          "x-go-name": "Graph"
        },
        "insight_type": {
          "$ref": "#/definitions/InsightType"
        },
        "name": {
          "type": "string",
          "x-go-name": "Name"
        },
        "priority": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Priority"
        },
        "title": {
          "type": "string",
          "x-go-name": "Title"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "InsightType": {
      "$ref": "#/definitions/Insight_INSIGHT_TYPE"
    },
    "Insight_INSIGHT_TYPE": {
      "type": "integer",
      "format": "int32",
      "x-go-package": "biostrap/proto"
    },
    "Insights": {
      "type": "object",
      "properties": {
        "insights": {
          "type": "object",
          "additionalProperties": {
            "$ref": "#/definitions/Insight"
          },
          "x-go-name": "Insights"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "InsightsResponseBody": {
      "type": "object",
      "properties": {
        "data": {
          "$ref": "#/definitions/Insights"
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "MonitoringInfo": {
      "type": "object",
      "required": [
        "subject_id",
        "subject_email",
        "subject_name"
      ],
      "properties": {
        "subject_email": {
          "description": "The subject's email address.",
          "type": "string",
          "x-go-name": "Email",
          "example": "john.doe@example.com"
        },
        "subject_id": {
          "description": "The subject's ID.",
          "type": "string",
          "x-go-name": "ID"
        },
        "subject_name": {
          "description": "The subject's name.",
          "type": "string",
          "x-go-name": "Name",
          "example": "John Doe"
        },
        "subject_photo_url": {
          "description": "URL to the subject's profile photo, if any.",
          "type": "string",
          "x-go-name": "PhotoURL",
          "example": "http://"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "MonitoringInfoResponseBody": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/MonitoringInfo"
          },
          "x-go-name": "Data"
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "PaginationLinks": {
      "description": "Links for resource pagination",
      "type": "object",
      "properties": {
        "next": {
          "description": "URL to next page",
          "type": "string",
          "x-go-name": "Next",
          "example": "https://..."
        },
        "prev": {
          "description": "URL to previous page",
          "type": "string",
          "x-go-name": "Prev",
          "example": "https://..."
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "ParameterErrorSource": {
      "type": "object",
      "properties": {
        "Reason": {
          "type": "string"
        },
        "parameter": {
          "description": "The query string parameter that caused the error",
          "type": "string",
          "x-go-name": "Parameter"
        }
      },
      "x-go-package": "biostrap/api/common/jsoncommon"
    },
    "PhysicalStats": {
      "type": "object",
      "properties": {
        "distance": {
          "description": "Meters walked in that minute.",
          "type": "number",
          "format": "float",
          "x-go-name": "Distance",
          "example": 79.2
        },
        "steps": {
          "description": "Number of steps walked in that minute.",
          "type": "integer",
          "format": "int32",
          "x-go-name": "Steps",
          "example": 110
        },
        "timestamp": {
          "description": "Timestamp is precise to the minute.",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Timestamp",
          "example": 1523905140
        },
        "total_step_calories": {
          "description": "Calories burnt from walking in that minute.",
          "type": "number",
          "format": "float",
          "x-go-name": "TotalStepCalories",
          "example": 28.2
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SleepDisturbanceGraph": {
      "type": "object",
      "properties": {
        "mild": {
          "$ref": "#/definitions/SleepDisturbanceGraphStageInfo"
        },
        "moderate": {
          "$ref": "#/definitions/SleepDisturbanceGraphStageInfo"
        },
        "none": {
          "$ref": "#/definitions/SleepDisturbanceGraphStageInfo"
        },
        "severe": {
          "$ref": "#/definitions/SleepDisturbanceGraphStageInfo"
        },
        "stages": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SleepDisturbanceStageSegment"
          },
          "x-go-name": "Stages"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SleepDisturbanceGraphStageInfo": {
      "type": "object",
      "properties": {
        "minutes_spent_in_stage": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "MinutesSpentInStage"
        },
        "percentage": {
          "type": "number",
          "format": "float",
          "x-go-name": "Percentage"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SleepDisturbanceStageSegment": {
      "type": "object",
      "properties": {
        "disturbance": {
          "type": "string",
          "x-go-name": "Disturbance"
        },
        "point": {
          "$ref": "#/definitions/TimeValuePoint"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SleepDisturbances": {
      "type": "object",
      "properties": {
        "arm": {
          "$ref": "#/definitions/SleepDisturbanceGraph"
        },
        "leg": {
          "$ref": "#/definitions/SleepDisturbanceGraph"
        },
        "snoring": {
          "$ref": "#/definitions/SleepDisturbanceGraph"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SleepScore": {
      "type": "object",
      "properties": {
        "message": {
          "description": "An explanation accompanying score.",
          "type": "string",
          "x-go-name": "Message"
        },
        "meta": {
          "description": "Values used to compute the score.",
          "type": "object",
          "additionalProperties": {
            "type": "number",
            "format": "float"
          },
          "x-go-name": "Meta"
        },
        "value": {
          "description": "The numeric value for the score.",
          "type": "integer",
          "format": "int32",
          "x-go-name": "Value"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SleepStage": {
      "$ref": "#/definitions/SleepTags"
    },
    "SleepStats": {
      "type": "object",
      "properties": {
        "arousal_count": {
          "description": "Number of times the user woke up.",
          "type": "integer",
          "format": "int32",
          "x-go-name": "ArousalCount"
        },
        "avg_heart_rate": {
          "description": "Average heart rate for the entire session.",
          "type": "integer",
          "format": "int32",
          "x-go-name": "AvgHeartRate"
        },
        "awake_time_mins": {
          "description": "Minutes awake.",
          "type": "integer",
          "format": "int32",
          "x-go-name": "AwakeTime"
        },
        "biometrics": {
          "$ref": "#/definitions/CardioStats"
        },
        "deep_sleep_mins": {
          "description": "Minutes spent in deep sleep.",
          "type": "integer",
          "format": "int32",
          "x-go-name": "DeepSleep"
        },
        "disturbances": {
          "$ref": "#/definitions/SleepDisturbances"
        },
        "end_timestamp": {
          "description": "End of sleep session.",
          "type": "integer",
          "format": "int64",
          "x-go-name": "EndTimestamp",
          "example": 1523946621
        },
        "fall_asleep_mins": {
          "description": "Minutes spent trying to fall asleep.",
          "type": "integer",
          "format": "int32",
          "x-go-name": "FallAsleep"
        },
        "light_sleep_mins": {
          "description": "Minutes spent in light sleep.",
          "type": "integer",
          "format": "int32",
          "x-go-name": "LightSleep"
        },
        "meta": {
          "description": "Metadata related to the recording.",
          "type": "object",
          "additionalProperties": {
            "type": "integer",
            "format": "int32"
          },
          "x-go-name": "Meta"
        },
        "score": {
          "$ref": "#/definitions/SleepScore"
        },
        "sleep_onset": {
          "description": "Timestamp for the first time the user fell asleep.",
          "type": "integer",
          "format": "int64",
          "x-go-name": "SleepOnset"
        },
        "stages": {
          "description": "An array containing the different stages the user\nwas in during the session.\nThe values contained in the array can be \"awake\",\n\"light_sleep\", \"deep_sleep\" and \"unknown\".",
          "type": "array",
          "items": {
            "$ref": "#/definitions/SleepStage"
          },
          "x-go-name": "Stages",
          "example": "light_sleep"
        },
        "start_timestamp": {
          "description": "Beginning of sleep session.",
          "type": "integer",
          "format": "int64",
          "x-go-name": "StartTimestamp",
          "example": 1523924112
        },
        "survey": {
          "$ref": "#/definitions/Survey"
        },
        "tz_offset": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "TzOffset"
        },
        "wakeup_time": {
          "description": "Timestamp for the last time the user woke up.",
          "type": "integer",
          "format": "int64",
          "x-go-name": "WakeUpTime"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SleepStatsResponseBody": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SleepStats"
          },
          "x-go-name": "Data"
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "SleepTags": {
      "description": "Sleep labels (tags) for the activeness bars shown on the sleep page. One tag per bar",
      "type": "integer",
      "format": "int32",
      "x-go-package": "biostrap/proto"
    },
    "StepsResponseBody": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/PhysicalStats"
          },
          "x-go-name": "Data"
        },
        "links": {
          "$ref": "#/definitions/PaginationLinks"
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "Survey": {
      "type": "object",
      "properties": {
        "date": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Date"
        },
        "questions": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/SurveyQuestion"
          },
          "x-go-name": "Questions"
        },
        "survey_id": {
          "type": "string",
          "x-go-name": "SurveyID"
        },
        "survey_type": {
          "$ref": "#/definitions/SurveyType"
        },
        "timestamp": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "Timestamp"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SurveyQuestion": {
      "type": "object",
      "properties": {
        "answer_percentage": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "x-go-name": "AnswerStrings"
        },
        "answer_strings": {
          "type": "number",
          "format": "float",
          "x-go-name": "AnswerPercentage"
        },
        "id": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "ID"
        },
        "question_text": {
          "type": "string",
          "x-go-name": "QuestionText"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "SurveyType": {
      "$ref": "#/definitions/SurveyType"
    },
    "SwimMetrics": {
      "type": "object",
      "properties": {
        "average_lap_time": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "AverageLapTime"
        },
        "calories_per_lap": {
          "type": "array",
          "items": {
            "type": "number",
            "format": "float"
          },
          "x-go-name": "CaloriesPerLap"
        },
        "laps": {
          "type": "number",
          "format": "float",
          "x-go-name": "Laps"
        },
        "length_time": {
          "type": "array",
          "items": {
            "type": "integer",
            "format": "int32"
          },
          "x-go-name": "LengthTime"
        },
        "pool_length": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "PoolLength"
        },
        "strokes_per_lap": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "StrokesPerLap"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "TimeValuePoint": {
      "type": "object",
      "properties": {
        "timestamp": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "Timestamp"
        },
        "timezone": {
          "type": "integer",
          "format": "int32",
          "x-go-name": "Timezone"
        },
        "value": {
          "type": "number",
          "format": "float",
          "x-go-name": "Value"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    },
    "UserNotFoundResponseBody": {
      "type": "object",
      "properties": {
        "detail": {
          "type": "string",
          "x-go-name": "Detail"
        },
        "status": {
          "description": "The HTTP status code for this error.",
          "type": "integer",
          "format": "int64",
          "x-go-name": "Status"
        },
        "title": {
          "type": "string",
          "x-go-name": "Title"
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "UserResponseBody": {
      "type": "object",
      "properties": {
        "data": {
          "$ref": "#/definitions/UserResponseData"
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "UserResponseData": {
      "type": "object",
      "required": [
        "id",
        "name",
        "email",
        "birthday",
        "gender",
        "height",
        "weight",
        "goals"
      ],
      "properties": {
        "birthday": {
          "type": "string",
          "x-go-name": "Birthday",
          "example": "1986-02-08"
        },
        "email": {
          "type": "string",
          "x-go-name": "Email",
          "example": "sameer@biostrap.com"
        },
        "gender": {
          "description": "'male', 'female' or 'undisclosed'",
          "type": "string",
          "x-go-name": "Gender",
          "example": "male"
        },
        "goals": {
          "$ref": "#/definitions/Goals"
        },
        "height": {
          "description": "In centimeters",
          "type": "number",
          "format": "float",
          "x-go-name": "Height",
          "example": 180
        },
        "id": {
          "type": "string",
          "x-go-name": "ID"
        },
        "name": {
          "type": "string",
          "x-go-name": "Name",
          "example": "Sameer Sontakey"
        },
        "photo_url": {
          "description": "User photo URL",
          "type": "string",
          "x-go-name": "PhotoURL",
          "example": "http://..."
        },
        "weight": {
          "description": "In kilograms",
          "type": "number",
          "format": "float",
          "x-go-name": "Weight",
          "example": 66
        }
      },
      "x-go-package": "biostrap/api/publicapi"
    },
    "WorkoutStats": {
      "type": "object",
      "properties": {
        "activities": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Activity"
          },
          "x-go-name": "Activities"
        },
        "name": {
          "type": "string",
          "x-go-name": "Name"
        },
        "timestamp": {
          "type": "integer",
          "format": "int64",
          "x-go-name": "Timestamp"
        }
      },
      "x-go-package": "biostrap/api/publicapi/publicapimodels"
    }
  },
  "responses": {
    "activitiesParameterErrorResponse": {
      "description": "Parameter error",
      "schema": {
        "type": "object",
        "properties": {
          "errors": {
            "type": "array",
            "items": {
              "$ref": "#/definitions/GenericParameterError"
            },
            "x-go-name": "Errors"
          }
        }
      }
    },
    "activitiesResponse": {
      "description": "A page of per-minute workout stats",
      "schema": {
        "$ref": "#/definitions/ActivitiesResponseBody"
      }
    },
    "biometricsResponse": {
      "description": "A page of biometrics.",
      "schema": {
        "$ref": "#/definitions/BiometricsResponseBody"
      }
    },
    "genericParameterErrorResponse": {
      "description": "Parameter error",
      "schema": {
        "type": "object",
        "properties": {
          "errors": {
            "type": "array",
            "items": {
              "$ref": "#/definitions/GenericParameterError"
            },
            "x-go-name": "Errors"
          }
        }
      }
    },
    "insightsResponse": {
      "description": "Insights for a given day.",
      "schema": {
        "$ref": "#/definitions/InsightsResponseBody"
      }
    },
    "monitoringInfoResponse": {
      "description": "List of users you monitor and their information.",
      "schema": {
        "$ref": "#/definitions/MonitoringInfoResponseBody"
      }
    },
    "sleepStatsResponse": {
      "description": "A list of sleep stats",
      "schema": {
        "$ref": "#/definitions/SleepStatsResponseBody"
      }
    },
    "stepsParameterErrorResponse": {
      "description": "Parameter error",
      "schema": {
        "type": "object",
        "properties": {
          "errors": {
            "type": "array",
            "items": {
              "$ref": "#/definitions/GenericParameterError"
            },
            "x-go-name": "Errors"
          }
        }
      }
    },
    "stepsResponse": {
      "description": "A page of per-minute physical stats",
      "schema": {
        "$ref": "#/definitions/StepsResponseBody"
      }
    },
    "userNotFoundResponse": {
      "description": "User not found",
      "schema": {
        "type": "object",
        "properties": {
          "errors": {
            "type": "array",
            "items": {
              "$ref": "#/definitions/UserNotFoundResponseBody"
            },
            "x-go-name": "Errors"
          }
        }
      }
    },
    "userResponse": {
      "description": "User profile",
      "schema": {
        "$ref": "#/definitions/UserResponseBody"
      }
    }
  },
  "securityDefinitions": {
    "OAuth2": {
      "type": "oauth2",
      "flow": "accessCode",
      "authorizationUrl": "https://auth.biostrap.com/authorize",
      "tokenUrl": "https://auth.biostrap.com/token",
      "scopes": {
        "": "Scopes are currently unused."
      }
    }
  }
}
